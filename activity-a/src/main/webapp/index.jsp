<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.Date" import="java.time.*" import="java.time.format.*" %>
    
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<title>Activity A</title>
	</head>
	<body>
		<h1>Our Date and Time now is...</h1>
		
		<%
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime japan = now.plusHours(1);
		LocalDateTime germany = now.minusHours(7);
		String currentTime = now.format(formatter);
		String japanTime = japan.format(formatter);
		String germanyTime = germany.format(formatter);
		%>

	<ul>
		<li> Manila: <%=currentTime%> </li>
		<li> Japan: <%=japanTime%>  </li>
		<li> Germany: <%=germanyTime%>  </li>
	</ul>
	
	<!-- Given the following Java Syntax below, apply the correct JSP syntax -->
	<%!
	private int initVar= 3;
	private int serviceVar= 3;
	private int destroyVar= 3;
	%>
	<%!
  	public void jspInit(){
    	initVar--;
    	System.out.println("jspInit(): init:" +initVar);
  		}
  	public void jspDestroy(){
    	destroyVar--;
    	System.out.println("jspDestroy(): destroy:" +destroyVar);
  	}
	%>
	<% 
  	serviceVar--;
  	System.out.println("_jspService(): service:" +serviceVar);
  	String content1="content1: "+initVar;
  	String content2="content2: "+serviceVar;
  	String content3="content3: "+destroyVar;
	%>
	<h1>JSP</h1>
	<p><%=content1%></p>
	<p><%=content2%></p>
	<p><%=content3%></p>
		
	</body>
</html>